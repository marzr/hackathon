var app = angular.module('main', ['ngRoute', 'ngResource', 'ngMaterial', 'ngMessages']);

app.config(function($routeProvider) {

    $routeProvider

        .when ('/', {
            templateUrl: 'templates/mainPage.html',
            controller: 'mainPageController'
        })
        
        .otherwise({redirectTo: '/'});
});



