angular.module('main')
    .controller('mainPageController', function($scope, $http) {

        const CRITERIA_ARRAY = [
                'CRIMINAL',
                'TRANSPORT_AVAILABILITY',
                'PARKINGS',
                'CINEMAS',
                'SHOPS',
                'CAFE',
                'BARS',
                'SHAWARMA',
                'BANKS',
                'POST',
                'HOSPITALS',
                'DENTISTRY',
                'MUSEUMS',
                'SCHOOLS',
                'THEATERS',
                'PLACES_FOR_REST'
            ],
            POST_LOCATIONS_BY_CRITERIA = 'http://localhost:8080/rank';

        $scope.criteriaArray = [];
        $scope.chosenOptions = [];

        (function fillCriteriaArray() {
            CRITERIA_ARRAY.forEach((item) => {
                $scope.criteriaArray.push(
                    {
                        criterion: item,
                        positive: true,
                        weightingFactor: 0
                    }
                );
            });
        })();

        function moveElementToAnotherArray(element, array1, array2) {
            let index = array2.indexOf(element);
            array1.push(element);
            if (index >= 0) {
                array2.splice(index, 1);
            }
        }

        $scope.addOption = (criteria) => {
            moveElementToAnotherArray(criteria, $scope.chosenOptions, $scope.criteriaArray);
        };

        $scope.deleteOption = (criteria) => {
            moveElementToAnotherArray(criteria, $scope.criteriaArray, $scope.chosenOptions);
        };

        let collectCriteriaData = () => {
            let result = [];
            $scope.chosenOptions.forEach((option) => {
                $scope.criteriaArray.push(option);
                return result.push({
                    'criterion': option.criterion,
                    'positive': option.positive ? 1 : 0,
                    'weightingFactor': option.weightingFactor / 10
                });
            });
            $scope.chosenOptions = [];
            return result;
        };

        $scope.sendCriteria = () => {
            $http.post(POST_LOCATIONS_BY_CRITERIA, collectCriteriaData())
                .then(
                    function(response) {
                        console.log(response);
                    },
                    function(response) {
                        console.log(response);
                    }
                )
        };



        const GET_LOCATION_DATA_URL_FROM_YANDEX = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=',
            GET_LOCATION_INFO_URL_FROM_SERVER = 'http://localhost:8080/homeDetails?';

        ymaps.ready(init);

        $scope.address = '';
        $scope.coords = [];
        $scope.locationInfo = [
            // {
            //     'criterion': 'CRIMINAL',
            //     'rating': 2.5,
            //     'stat': {
            //         'min': 3,
            //         'max': 5,
            //         'median': 4,
            //         'average': 5
            //     },
            //     'value': 5
            // },
            // {
            //     'criterion': 'CAFES',
            //     'rating': 2.5,
            //     'stat': {
            //         'min': 3,
            //         'max': 5,
            //         'median': 4,
            //         'average': 5
            //     },
            //     'value': 5
            // }
        ];

        let map, placemark;

        function init() {
            map = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 7
            });

            map.controls.remove('searchControl');
        }

        function parseCoords(locationObject) {
            let coordsArray = locationObject.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(" "),
                result = [];
            coordsArray.forEach((coord)=> {
                result.push(+coord);
            });
            console.log('RESULT' + result);
            return result;
        }

        function getLocationInfo(latitude, longitude) {
            $http.get(GET_LOCATION_INFO_URL_FROM_SERVER + 'lat=' + latitude + '&lon=' + longitude).
            then (
                function(response) {
                    $scope.locationInfo = response.data;
                },
                function(response) {
                    console.log(response);
                }
            )
        }

        function getCoords(address) {
            $http.get(GET_LOCATION_DATA_URL_FROM_YANDEX + address)
                .then(
                    function(response) {
                        $scope.coords = parseCoords(response.data.response).slice(0);
                        getLocationInfo($scope.coords[1], $scope.coords[0]);
                        if ($scope.coords.length !== 0) {
                            map.setCenter([$scope.coords[1], $scope.coords[0]], 15);
                        }
                        map.geoObjects.remove(placemark);
                        placemark = new ymaps.Placemark([$scope.coords[1], $scope.coords[0]]);
                        map.geoObjects.add(placemark);

                    },
                    function(response) {
                        console.log(response);
                    }
                )
        }

        $scope.redrawMap = () => {
            getCoords($scope.address);
        };


    });