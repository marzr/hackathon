angular.module('main')

    .directive('header', function() {
        return {
            restrict: 'AE',
            bindToController: true,
            controller: 'headerController',
            templateUrl: './templates/header.html'
        };

    })

    .controller('headerController', function($http){
        
    });

