package com.epam.dsh.geobobry.decisionmaking;

import com.epam.dsh.geobobry.entities.RankingCriterion;
import com.epam.dsh.geobobry.enums.CriterionEnum;
import com.jayway.jsonpath.Criteria;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;

import static org.junit.Assert.*;

public class MethodOfLinearConvolutionTest {

    @Test
    public void choose() {
        List<Integer> expected = new ArrayList<Integer>() {{
            add(0);
            add(1);
        }};

        List<RankingCriterion> criteria = Arrays.asList(
                new RankingCriterion(CriterionEnum.CAFE, true, 0.9),
                new RankingCriterion(CriterionEnum.BANKS, true, 0.1));

        Map<Integer, Map<CriterionEnum, Double>> inputHomes = new HashMap<Integer, Map<CriterionEnum, Double>>() {{
            put(0, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.8);
                put(CriterionEnum.BANKS, 0.4);
            }});
            put(1, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.2);
                put(CriterionEnum.BANKS, 0.9);
            }});
        }};

        MethodOfLinearConvolution methodOfLinearConvolution = new MethodOfLinearConvolution();
        List<Integer> actual = methodOfLinearConvolution.choose(criteria, inputHomes);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), actual.get(i));
        }
    }
}