package com.epam.dsh.geobobry.mongoconnect;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class MongoServiceTest {

    MongoService mongoService = new MongoService();


    @Test
    public void evaluateAddress() { //  59.9291272	30.2497768

        double lat = 59.9291272;
        double lon = 30.2497768;
        List<Double> coords = Arrays.asList(lat, lon);
        Map<String, Integer> stringIntegerMap = mongoService.evaluateAddress(coords);
        System.out.println(stringIntegerMap);
    }

    @Test
    public void getHomes() {
        System.out.println(mongoService.getHomes());
    }
}