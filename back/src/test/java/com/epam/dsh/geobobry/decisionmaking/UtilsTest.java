package com.epam.dsh.geobobry.decisionmaking;

import static javax.swing.UIManager.put;
import static org.junit.Assert.*;

import com.epam.dsh.geobobry.enums.CriterionEnum;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


public class UtilsTest {

    @Test
    public void tetsNormalize() {
        Map<Integer, Map<CriterionEnum, Double>> input = new HashMap<Integer, Map<CriterionEnum, Double>>() {{
            put(0, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.8);
                put(CriterionEnum.BANKS, 0.4);
            }});
            put(1, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.5);
                put(CriterionEnum.BANKS, 0.9);
            }});
        }};
        Map<Integer, Map<CriterionEnum, Double>> expected = new HashMap<Integer, Map<CriterionEnum, Double>>() {{
            put(0, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.89443);
                put(CriterionEnum.BANKS, 0.44721);
            }});
            put(1, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.48564);
                put(CriterionEnum.BANKS, 0.87416);
            }});
        }};
        Map<Integer, Map<CriterionEnum, Double>> actual = Utils.normalize(input);

        assertEquals(expected.get(0).get(CriterionEnum.CAFE), actual.get(0).get(CriterionEnum.CAFE), 0.00001);
        assertEquals(expected.get(0).get(CriterionEnum.BANKS), actual.get(0).get(CriterionEnum.BANKS), 0.00001);
        assertEquals(expected.get(1).get(CriterionEnum.CAFE), actual.get(1).get(CriterionEnum.CAFE), 0.00001);
        assertEquals(expected.get(1).get(CriterionEnum.BANKS), actual.get(1).get(CriterionEnum.BANKS), 0.00001);
    }

    @Test
    public void testLinearConvolution() {
        Map<CriterionEnum, Double> alphaInput = new HashMap<CriterionEnum, Double>(){{
            put(CriterionEnum.BANKS, 0.8);
            put(CriterionEnum.CAFE, 0.6);
        }};
        Map<CriterionEnum, Double> nInput = new HashMap<CriterionEnum, Double>(){{
            put(CriterionEnum.BANKS, 0.5);
            put(CriterionEnum.CAFE, 0.25);
        }};

        assertEquals(0.55, Utils.linearConvolution(alphaInput, nInput), 0.000001);
    }
}