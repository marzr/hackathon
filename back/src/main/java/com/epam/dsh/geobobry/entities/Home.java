package com.epam.dsh.geobobry.entities;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName
public class Home {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    int id;
    double lat;
    double lon;
    String address;

    public Home() {

    }
    public Home(int id) {
        this.id = id;
    }

}
