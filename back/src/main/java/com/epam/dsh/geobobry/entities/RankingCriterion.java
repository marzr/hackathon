package com.epam.dsh.geobobry.entities;

import com.epam.dsh.geobobry.enums.CriterionEnum;
public class RankingCriterion {
    CriterionEnum criterion;
    boolean positive;
    double weightingFactor;

    public RankingCriterion(CriterionEnum criterion, boolean positive, double weightingFactor) {
        this.criterion = criterion;
        this.positive = positive;
        this.weightingFactor = weightingFactor;
    }

    public RankingCriterion() {

    }

    public double getWeightingFactor() {
        return weightingFactor;
    }

    public void setWeightingFactor(double weightingFactor) {
        this.weightingFactor = weightingFactor;
    }

    public CriterionEnum getCriterion() {
        return criterion;
    }

    public void setCriterion(CriterionEnum criterion) {
        this.criterion = criterion;
    }

    public boolean getPositive() {
        return positive;
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    @Override
    public String toString() {
        return "RankingCriterion{" +
                "criterion=" + criterion +
                ", positive=" + positive +
                ", weightingFactor=" + weightingFactor +
                '}';
    }
}
