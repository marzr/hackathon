package com.epam.dsh.geobobry.decisionmaking;

import com.epam.dsh.geobobry.enums.CriterionEnum;

import java.util.HashMap;
import java.util.Map;

public abstract class Utils {
    public static Map<Integer, Map<CriterionEnum, Double>> normalize(final Map<Integer, Map<CriterionEnum, Double>> sourceData) {
        Map<Integer, Map<CriterionEnum, Double>> normalizedData = new HashMap<>();

        sourceData.forEach((id, element) -> {
            double magnitude = 0;
            for (Map.Entry<CriterionEnum, Double> entry : element.entrySet()) {
                Double value = entry.getValue();
                magnitude += value * value;
            }
            magnitude = Math.sqrt(magnitude);

            Map<CriterionEnum, Double> normalizedCriterionMap = new HashMap<>();
            for (Map.Entry<CriterionEnum, Double> entry : element.entrySet()) {
                CriterionEnum criterion = entry.getKey();
                Double value = entry.getValue();
                normalizedCriterionMap.put(criterion, (double) value / magnitude);
            }
            normalizedData.put(id, normalizedCriterionMap);
        });

        return normalizedData;
    }

    public static double linearConvolution(final Map<CriterionEnum, Double> alpha,
                                           final Map<CriterionEnum, Double> n) {
        double result = 0;
        for (Map.Entry<CriterionEnum, Double> entry : n.entrySet()) {
            CriterionEnum criterion = entry.getKey();

            double ni = entry.getValue();
            double alphai = alpha.containsKey(criterion) ? alpha.get(criterion): 0;

            result += ni * alphai;
        }

        return result;
    }
}
