package com.epam.dsh.geobobry.controllers;

import com.epam.dsh.geobobry.entities.HomeCriterionDetail;
import com.epam.dsh.geobobry.entities.Statistics;
import com.epam.dsh.geobobry.enums.CriterionEnum;
import com.epam.dsh.geobobry.mongoconnect.MongoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
public class HomeDetailsController {

    MongoService mongoService = new MongoService();

    @RequestMapping("/homeDetails")
    public List<HomeCriterionDetail> rankingQueries(@RequestParam(name = "lat") double lat,
                                                    @RequestParam(name = "lon") double lon) {

        List<HomeCriterionDetail> result = new ArrayList<>();
        Map<String, Integer> scores = mongoService.evaluateAddress(Arrays.asList(lat, lon));

        for (CriterionEnum criterion : CriterionEnum.values()) {
            System.out.println("scores !!!!! "+scores);
            System.out.println("criterion !!!!" + criterion.name());
            Integer rating = scores.get(criterion.name() + "__rating");
            Integer value = scores.get(criterion.name() + "__total");
            Statistics statistics = new Statistics(); //TODO get from mongo Get Statistics for this criterion
            //int value = 0; //TODO get neib using spatial query for this lat, lon Example: count of crimes around this building
            Map<String, Double> avgValues = mongoService.getAverages(criterion.name());
            statistics.setAverage(avgValues.get("average"));
            statistics.setMax(avgValues.get("max").intValue());
            statistics.setMin(avgValues.get("min").intValue());
            statistics.setMedian(avgValues.get("median").intValue());
            HomeCriterionDetail detail = new HomeCriterionDetail();
            detail.setRating(rating);
            detail.setCriterion(criterion);
            detail.setStat(statistics);
            detail.setValue(value);
            result.add(detail);
        }

        return result;
    }

    private int calculateRating(Statistics statistics, double value) {
        return 0;
    }
}
