package com.epam.dsh.geobobry.entities;

import java.util.List;

public class Histogram {
    List<Integer> columns;
    int selectedColumn;

    public List<Integer> getColumns() {
        return columns;
    }

    public void setColumns(List<Integer> columns) {
        this.columns = columns;
    }

    public int getSelectedColumn() {
        return selectedColumn;
    }

    public void setSelectedColumn(int selectedColumn) {
        this.selectedColumn = selectedColumn;
    }
}
