package com.epam.dsh.geobobry.decisionmaking;

import com.epam.dsh.geobobry.entities.RankingCriterion;
import com.epam.dsh.geobobry.enums.CriterionEnum;
import org.springframework.util.CollectionUtils;

import java.util.*;

public class MethodOfLinearConvolution {
    public List<Integer> choose(final List<RankingCriterion> criteria,
                                final Map<Integer, Map<CriterionEnum, Double>> criteriaValues) {
        List<Integer> resultHouseIds = new ArrayList<>();

        Map<Integer, Map<CriterionEnum, Double>> normalizedCriteriaValues = Utils.normalize(criteriaValues);
        Map<CriterionEnum, Double> weightingFactors = retrieveWeightingFactorsMap(criteria);

        TreeSet<IdAndScore> scores = new TreeSet<>();
        normalizedCriteriaValues.forEach((elementId, criteriaMap) ->
            scores.add(new IdAndScore(elementId, Utils.linearConvolution(weightingFactors, criteriaMap))));

        int num = 10;
        int id = 0;
        for (IdAndScore idAndScore : scores) {
            if (id >= num) break;
            resultHouseIds.add(idAndScore.id);
            id++;
        }

        return resultHouseIds;
    }

    protected Map<CriterionEnum, Double> retrieveWeightingFactorsMap(final List<RankingCriterion> criteria) {
        Map<CriterionEnum, Double> weightingFactors = new HashMap<>();

        for (RankingCriterion criterion : criteria) {
            weightingFactors.put(criterion.getCriterion(),
                    criterion.getPositive() ? criterion.getWeightingFactor() : - criterion.getWeightingFactor());
        }

        return weightingFactors;
    }

    private static class IdAndScore implements Comparable<IdAndScore> {
        Integer id;
        Double score;

        public IdAndScore(Integer id, Double score) {
            this.id = id;
            this.score = score;
        }

        @Override
        public int compareTo(IdAndScore other) {
            return  - Double.compare(this.score, other.score);
        }
    }

//    protected Map<CriterionEnum, Double> weightingFactorsCalculation(final List<Criterion> criteria) {
//        Map<CriterionEnum, Double> weightingFactors = new HashMap<>();
//
//        double currentWeightingFactor = 0.5;
//        for (Criterion criterion : criteria) {
//            weightingFactors.put(criterion.criterion,
//                                 criterion.isPositive ? currentWeightingFactor : - currentWeightingFactor);
//
//            currentWeightingFactor = currentWeightingFactor / 2;
//        }
//
//        return weightingFactors;
//    }
}
