package com.epam.dsh.geobobry.mongoconnect;

import com.epam.dsh.geobobry.enums.Criteria;
import com.epam.dsh.geobobry.enums.CriterionEnum;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MongoService {

    private String mongoHost = "192.168.128.96";
    private String databaseName = "local";
    private MongoClient mongoClient;
    private MongoDatabase database;

    public MongoService() {
        try  {
            this.mongoClient = new MongoClient(mongoHost, 27017);
            this.database = mongoClient.getDatabase(databaseName);
        }
        catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    public Map<String, Double> getAverages(String type) {
        final Map<String, Double> scores = new HashMap<>();
        FindIterable<Document> averages = database.getCollection("averages").find();
        for (Document average : averages) {
            System.out.println(average);
            if (average.get("type").equals(type)) {
                scores.put("max", (Double) average.get("max"));
                scores.put("min", (Double) average.get("min"));
                scores.put("median", average.getInteger("median").doubleValue());
                scores.put("average", (Double) average.get("average"));
                return scores;
            }
        }
        return scores;
    }

    private Document getNeighbors(String[] coordinates, String collectionName, double distanceKm) {
        double[] coords = {Double.valueOf(coordinates[0]), Double.valueOf(coordinates[1])};
        BasicDBObject myCmd = new BasicDBObject();
        myCmd.append("geoNear", collectionName);
        myCmd.append("near", coords);
        myCmd.append("spherical", true);
        myCmd.append("limit", 1000);
        myCmd.append("maxDistance", distanceKm / 6378); // Earth radius
        //System.out.println(myCmd);
        Document result = this.database.runCommand(myCmd);
        //System.out.println(result.toString());
        return result;
    }

    public int analyzeCriteria(List<Double> coords, String documentType, Criteria criteria) {
        Document results = getNeighbors(coords, documentType, 0.5, criteria);
        ArrayList cRes = (ArrayList) results.get("results");
        return cRes.size();
    }

    public Map<String, Integer> evaluateAddress(List<Double> coordinates) {
        Map<String, Integer> output = new HashMap<>();
        //MongoDatabase database = connector.connectToDb("geoData");
        for (Criteria criteria : Criteria.values()) {
            int result = analyzeCriteria(coordinates, criteria.collectionName, criteria);
            MongoCollection<Document> averages = database.getCollection("averages");
            FindIterable<Document> found = averages.find(Filters.eq("type", criteria.name()));
            String markKey = criteria.name() + "__rating";
            String totalKey = criteria.name() + "__total";
            output.put(totalKey, result);
            for(Document document: found) {
                Integer fi = (Integer) document.get("firstq");
                if (result < fi) {
                    output.put(markKey, 1);
                    break;
                }
                Integer sec = (Integer) document.get("secondq");
                if (result < sec) {
                    output.put(markKey, 2);
                    break;
                }
                Integer th = (Integer) document.get("thirdq");
                if (result < th) {
                    output.put(markKey, 3);
                    break;
                }
                Integer fo = (Integer) document.get("fourthq");
                if (result < th) {
                    output.put(markKey, 4);
                    break;
                }
                output.put(markKey, 5);
                break;
            }
        }
        return output;
    }

    public List<Document> getHomes() {
        List<Document> documents = new ArrayList<>();
        final int LIMIT = 10;

        MongoCollection<Document> homes = database.getCollection("homes");
        System.out.println("Starting...");
        MongoCursor<Document> homesIterable = homes.find().iterator();
        int i = 0;
        while (homesIterable.hasNext()) {
            Document home = homesIterable.next();
            System.out.println("******" + home.keySet());
            documents.add(home);
            if (i++ > LIMIT) return documents;

            System.out.println(home.get("values"));
        }
        return documents;
    }

    public Document getNeighbors(List<Double> coordinates, String collectionName, double distanceKm,
                                 Criteria criteria) {
        double[] coords = {coordinates.get(0), coordinates.get(1)};
        BasicDBObject myCmd = new BasicDBObject();

        myCmd.append("geoNear", collectionName);
        if (criteria != null && criteria.isSubtype) {
            BasicDBObject filter = new BasicDBObject("type", new BasicDBObject("$in", criteria.subtype));
            myCmd.append("query", filter);
        }
        myCmd.append("near", coords);
        myCmd.append("spherical", true);
        myCmd.append("limit", 1000);
        myCmd.append("maxDistance", distanceKm / 6378); // Earth radius
        System.out.println(myCmd);
        Document result = this.database.runCommand(myCmd);
        //System.out.println(result.toString());
        return result;
    }

}
