package com.epam.dsh.geobobry.entities;

import com.epam.dsh.geobobry.enums.CriterionEnum;

public class HomeCriterionDetail {

    private CriterionEnum criterion;
    private int rating;
    private Statistics stat;
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public CriterionEnum getCriterion() {
        return criterion;
    }

    public void setCriterion(CriterionEnum criterion) {
        this.criterion = criterion;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Statistics getStat() {
        return stat;
    }

    public void setStat(Statistics stat) {
        this.stat = stat;
    }
}
