package com.epam.dsh.geobobry.controllers;

import com.epam.dsh.geobobry.decisionmaking.MethodOfLinearConvolution;
import com.epam.dsh.geobobry.entities.Home;
import com.epam.dsh.geobobry.entities.RankingCriterion;
import com.epam.dsh.geobobry.enums.CriterionEnum;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class RankingController {

    @RequestMapping(value = "/rank",
            method = RequestMethod.POST)
    public List<Home> rankingQueries(@RequestBody List<RankingCriterion> criteria) {
        //System.out.println(criteria.get(0));
        criteria.forEach(System.out::println);

        Map<Integer, Map<CriterionEnum, Double>> inputHomes = new HashMap<Integer, Map<CriterionEnum, Double>>() {{
            put(0, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.8);
                put(CriterionEnum.BANKS, 0.4);
            }});
            put(1, new HashMap<CriterionEnum, Double>() {{
                put(CriterionEnum.CAFE, 0.2);
                put(CriterionEnum.BANKS, 0.9);
            }});
        }};

        MethodOfLinearConvolution methodOfLinearConvolution = new MethodOfLinearConvolution();
        List<Integer> chosenHomes = methodOfLinearConvolution.choose(criteria, inputHomes);

        return chosenHomes.stream().map(Home::new).collect(Collectors.toList());
    }
}
