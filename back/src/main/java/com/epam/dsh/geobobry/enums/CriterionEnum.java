package com.epam.dsh.geobobry.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CriterionEnum {
    @JsonProperty("CRIMINAL")
    CRIMINAL,
    @JsonProperty("TRANSPORT_AVAILABILITY")
    TRANSPORT_AVAILABILITY,
    @JsonProperty("PARKINGS")
    PARKINGS,
    @JsonProperty("CINEMAS")
    CINEMAS,
    @JsonProperty("SHOPS")
    SHOPS,
    @JsonProperty("CAFE")
    CAFE,
    @JsonProperty("BARS")
    BARS,
    @JsonProperty("SHAWARMA")
    SHAWARMA,
    @JsonProperty("BANKS")
    BANKS,
    @JsonProperty("POST")
    POST,
    @JsonProperty("HOSPITALS")
    HOSPITALS,
    @JsonProperty("DENTISTRY")
    DENTISTRY,
    @JsonProperty("MUSEUMS")
    MUSEUMS,
    @JsonProperty("SCHOOLS")
    SCHOOLS,
    @JsonProperty("THEATERS")
    THEATERS,
    @JsonProperty("PLACES_FOR_REST")
    PLACES_FOR_REST
}
