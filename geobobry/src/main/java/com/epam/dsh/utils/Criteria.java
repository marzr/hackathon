package com.epam.dsh.utils;

import java.util.Arrays;
import java.util.HashSet;

public enum Criteria {
    CRIMINAL ("crimes", false),
    TRANSPORT_AVAILABILITY ("amenities", true, "bus_station", "trolley_stop"),
    PARKINGS ("amenities", true, "parking", "parking_space", "parking_entrance"),
    CINEMAS ("amenities", true, "cinema"),
    SHOPS ("shops", false),
    CAFE ("amenities", true, "cafe"),
    BARS ("amenities", true, "bar", "pub"),
    SHAWARMA ("amenities", true, "fast_food"),
    BANKS ("amenities", true, "bank"),
    POST ("amenities", true, "post_office", "post_box"),
    HOSPITALS ("amenities", true, "hospital", "pharmacy"),
    DENTISTRY ("amenities", true, "dentist"),
    MUSEUMS ("amenities", true, "arts_centre"),
    SCHOOLS ("amenities", true, "school"),
    THEATERS ("amenities", true, "theatre"),
    PLACES_FOR_REST ("amenities", true, "nightclub");
    public final String collectionName;
    public final boolean isSubtype;
    public final HashSet<String> subtype = new HashSet<>();

    private Criteria(final String collectionName, final boolean isSubtype, final String... subtypes) {
        this.collectionName = collectionName;
        this.isSubtype = isSubtype;
        this.subtype.addAll(Arrays.asList(subtypes));
    }
}
