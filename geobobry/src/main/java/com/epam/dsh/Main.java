package com.epam.dsh;

import com.epam.dsh.configuration.AppConfiguration;
import com.epam.dsh.csvextract.CsvExtractor;
import com.epam.dsh.mongoconnect.AverageCalculator;
import com.epam.dsh.mongoconnect.MongoConnector;
import com.epam.dsh.utils.Criteria;
import com.mongodb.client.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.print.Doc;


@Component
public class Main {

    // TODO: move to properties
    @Value("D:\\hackathon_data\\data\\")
    private String csvPath;


    @Autowired
    private CsvExtractor csvExtractor;


    public static void main(String[] args) throws IOException {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        MongoConnector connector = (MongoConnector) context.getBean("mongoConnector");
        MongoDatabase database = connector.connectToDb("local");
        Main mainInstance = (Main) context.getBean("main");
        //mainInstance.csvExtractor.extractData(mainInstance.csvPath);
        System.out.println(database.getCollection("amenities").count());
        System.out.println(database.getCollection("homes").count());
        System.out.println(database.getCollection("crimes").count());
        System.out.println(database.getCollection("shops").count());
        AverageCalculator calculator = (AverageCalculator) context.getBean("averageCalculator");
        //calculator.calculateAverages();
        MongoCollection<Document> avgs = database.getCollection("averages");
        FindIterable<Document> iterable = avgs.find(new BsonDocument());
        for(Document document : iterable) {
            System.out.println(document);
        }

        List<Double> coords = new ArrayList<>();
        // 59.959789, 30.325450
        coords.add(59.959789);
        coords.add(30.325450);
        System.out.println(calculator.evaluateAddress(coords));
        //connector.getNeighbors(coords, "amenities", 1, Criteria.BARS);


/*
        Iterator i = database.getCollection("amenities").find().iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }
*/
    }

}
