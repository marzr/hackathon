package com.epam.dsh.mongoconnect;

import com.epam.dsh.utils.Criteria;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongoConnector {

    @Value("192.168.128.96")
    private String mongoHost;

    private MongoClient mongoClient;
    private MongoDatabase database;

    public MongoConnector() {
        try  {
            this.mongoClient = new MongoClient(mongoHost, 27017);
        }
        catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    public MongoConnector(String host, int port) {
        try  {
            this.mongoClient = new MongoClient(host, port);
        }
        catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    public MongoDatabase connectToDb(String dbName) {
        if (this.database == null) {
            this.database = this.mongoClient.getDatabase(dbName);
        }
        return this.database;
    }

    public Document getNeighbors(List<Double> coordinates, String collectionName, double distanceKm) {
        return getNeighbors(coordinates, collectionName, distanceKm, null);
    }

    public Document getNeighbors(List<Double> coordinates, String collectionName, double distanceKm,
                                 Criteria criteria) {
        double[] coords = {coordinates.get(0), coordinates.get(1)};
        BasicDBObject myCmd = new BasicDBObject();

        myCmd.append("geoNear", collectionName);
        if (criteria != null && criteria.isSubtype) {
            BasicDBObject filter = new BasicDBObject("type", new BasicDBObject("$in", criteria.subtype));
            myCmd.append("query", filter);
        }
        myCmd.append("near", coords);
        myCmd.append("spherical", true);
        myCmd.append("limit", 1000);
        myCmd.append("maxDistance", distanceKm / 6378); // Earth radius
        //System.out.println(myCmd);
        Document result = this.database.runCommand(myCmd);
        //System.out.println(result.toString());
        return result;
    }


}
