package com.epam.dsh.mongoconnect;

import com.epam.dsh.utils.Criteria;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class AverageCalculator {

    @Autowired
    MongoConnector connector;

    public void calculateAverages() {
        MongoDatabase database = connector.connectToDb("geoData");
        MongoCollection<Document> homes = database.getCollection("homes");
        int counter = 0;
        System.out.println("Starting...");
        MongoCursor<Document> homesIterable = homes.find().iterator();
        Map<String, List<Integer>> valuesForCriteria = new HashMap<>();
        int iii = 0;
        while (homesIterable.hasNext()) {
            if (++iii > 1000) break;

            Document home = homesIterable.next();
            Document geopoint = (Document) home.get("geopoint");
            List<Double> coords = (List<Double>) geopoint.get("coordinates");
            for(Criteria crit : Criteria.values()) {
                if (valuesForCriteria.get(crit.name()) == null) {
                    valuesForCriteria.put(crit.name(), new ArrayList<>());
                }
                int value = analyzeCriteria(coords, crit.collectionName.toLowerCase(), crit);
                homes.replaceOne(home, home.append(crit.name(), value));
                valuesForCriteria.get(crit.name()).add(value);
            }
            System.out.println(++counter + " done from " + homes.count());
        }
        System.out.println("Loaded data...");
        MongoCollection<Document> averages = database.getCollection("averages");
        averages.deleteMany(new BsonDocument());
        for(Map.Entry<String, List<Integer>> entry : valuesForCriteria.entrySet()) {
            Collections.sort(entry.getValue());
            List<Integer> filtered = entry.getValue().stream().filter( i -> i > 0).collect(Collectors.toList());
            double average = filtered.stream().mapToDouble(n -> n).average().getAsDouble();
            double max = filtered.stream().mapToDouble(n -> n).max().getAsDouble();
            double min = filtered.stream().mapToDouble(n -> n).min().getAsDouble();
            int median = filtered.get(filtered.size() / 2);
            int[] quantilesBorders = getQuantilesBorders(filtered);

            Document averageStats = new Document();
            averageStats.append("type", entry.getKey());
            averageStats.append("values", filtered);
            averageStats.append("average", average);
            averageStats.append("max", max);
            averageStats.append("min", min);
            averageStats.append("median", median);
            averageStats.append("firstq", quantilesBorders[0]);
            averageStats.append("secondq", quantilesBorders[1]);
            averageStats.append("thirdq", quantilesBorders[2]);
            averageStats.append("fourthq", quantilesBorders[3]);
            averages.insertOne(averageStats);
        }
        System.out.println("Loaded averages...");

    }

    private int[] getQuantilesBorders(List<Integer> value) {
        int border = value.size() / 5;
        int[] result = new int[4];
        for (int i = 0; i < 4; i++) {
            result[i] = value.get((i + 1) * border);
        }
        return result;
    }

    public int analyzeCriteria(List<Double> coords, String documentType, Criteria criteria) {
        Document results = connector.getNeighbors(coords, documentType, 0.5, criteria);
        ArrayList cRes = (ArrayList) results.get("results");
        return cRes.size();
    }

    public Map<String, Integer> evaluateAddress(List<Double> coordinates) {
        Map<String, Integer> output = new HashMap<>();
        MongoDatabase database = connector.connectToDb("geoData");
        for (Criteria criteria : Criteria.values()) {
            int result = analyzeCriteria(coordinates, criteria.collectionName, criteria);
            MongoCollection<Document> averages = database.getCollection("averages");
            FindIterable<Document> found = averages.find(Filters.eq("type", criteria.name()));
            for(Document document: found) {
                Integer fi = (Integer) document.get("firstq");
                if (result < fi) {
                    output.put(criteria.name(), 1);
                    break;
                }
                Integer sec = (Integer) document.get("secondq");
                if (result < sec) {
                    output.put(criteria.name(), 2);
                    break;
                }
                Integer th = (Integer) document.get("thirdq");
                if (result < th) {
                    output.put(criteria.name(), 3);
                    break;
                }
                Integer fo = (Integer) document.get("fourthq");
                if (result < th) {
                    output.put(criteria.name(), 4);
                    break;
                }
                output.put(criteria.name(), 5);
                break;
            }
        }
        return output;
    }

}
