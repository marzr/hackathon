package com.epam.dsh.configuration;

import com.epam.dsh.mongoconnect.MongoConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.epam.dsh")
@PropertySource("classpath:application.properties")
public class AppConfiguration {

}