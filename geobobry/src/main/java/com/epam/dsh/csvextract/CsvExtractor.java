package com.epam.dsh.csvextract;

public interface CsvExtractor {
    void extractData(String path);
}
