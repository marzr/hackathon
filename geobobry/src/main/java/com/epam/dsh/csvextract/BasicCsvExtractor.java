package com.epam.dsh.csvextract;

import com.epam.dsh.mongoconnect.MongoConnector;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class BasicCsvExtractor implements CsvExtractor {

    // TODO: move to properties
    @Value("OpenStreetMap\\amenities.csv")
    private String amenitiesPath;
    @Value("OpenData\\Объекты недвижимого имущества и земельные участки.csv")
    private String homesPath;
    @Value("MVD_News\\news_with_location.csv")
    private String crimesPath;
    @Value("OpenStreetMap\\shops.csv")
    private String shopsPath;
    @Value("Transport\\extra\\Данные.csv")
    private String stopsPath;

    @Autowired
    MongoConnector connector;


    @Override
    public void extractData(String path) {
        MongoDatabase database = connector.connectToDb("geoData");
        extractAmenities(path, database);
        extractCrimes(path, database);
        extractHomes(path, database);
        extractShops(path, database);
    }

    private void extractAmenities(String path, MongoDatabase database) {
        try (Reader in = new FileReader(path + amenitiesPath)){
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            List<Document> amenityList = new ArrayList<>();
            String collectionName = "amenities";
            try {
                database.createCollection(collectionName);
            } catch (Exception e) {
                System.out.println("Collection already exists: " + collectionName);
                System.out.println(database.getCollection(collectionName).deleteMany(new BsonDocument()));
            }
            for (CSVRecord record : records) {
                Document amenity = new Document();
                amenity.append("type", record.get(0));
                amenity.append("name", record.get(1));
                Double[] latLon = {Double.valueOf(record.get(2)), Double.valueOf(record.get(3))};
                List<Double> latitudeLongitude = Arrays.asList(latLon);
                Document point = new Document().append("type", "Point");
                point.append("coordinates", latitudeLongitude);
                amenity.append("geopoint", point);
                amenityList.add(amenity);
            }
            database.getCollection(collectionName).insertMany(amenityList);
            System.out.println(database.getCollection(collectionName).createIndex(Indexes.geo2dsphere("geopoint.coordinates")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void extractHomes(String path, MongoDatabase database) {
        try (Reader in = new FileReader(path + homesPath)){
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            List<Document> homesList = new ArrayList<>();
            String collectionName = "homes";
            try {
                database.createCollection(collectionName);
            } catch (Exception e) {
                System.out.println("Collection already exists: " + collectionName);
                System.out.println(database.getCollection(collectionName).deleteMany(new BsonDocument()));
            }
            for (CSVRecord record : records) {
                Document home = new Document();
                home.append("address", record.get(0));
                Double[] latLon = {Double.valueOf(record.get(8)), Double.valueOf(record.get(7))};
                List<Double> latitudeLongitude = Arrays.asList(latLon);
                Document point = new Document().append("type", "Point");
                point.append("coordinates", latitudeLongitude);
                home.append("geopoint", point);
                homesList.add(home);
            }
            database.getCollection(collectionName).insertMany(homesList);
            System.out.println(database.getCollection(collectionName).createIndex(Indexes.geo2dsphere("geopoint.coordinates")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void extractCrimes(String path, MongoDatabase database) {
        try (Reader in = new FileReader(path + crimesPath)){
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            List<Document> crimesList = new ArrayList<>();
            String collectionName = "crimes";
            try {
                database.createCollection(collectionName);
            } catch (Exception e) {
                System.out.println("Collection already exists: " + collectionName);
                System.out.println(database.getCollection(collectionName).deleteMany(new BsonDocument()));
            }
            for (CSVRecord record : records) {
                Document crime = new Document();
                crime.append("title", record.get(1));
                Double[] latLon = {Double.valueOf(record.get(3)), Double.valueOf(record.get(4))};
                List<Double> latitudeLongitude = Arrays.asList(latLon);
                Document point = new Document().append("type", "Point");
                point.append("coordinates", latitudeLongitude);
                crime.append("geopoint", point);
                crime.append("type", record.get(5));
                crimesList.add(crime);
            }
            database.getCollection(collectionName).insertMany(crimesList);
            System.out.println(database.getCollection(collectionName).createIndex(Indexes.geo2dsphere("geopoint.coordinates")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void extractShops(String path, MongoDatabase database) {
        try (Reader in = new FileReader(path + shopsPath)) {
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            List<Document> shopsList = new ArrayList<>();
            String collectionName = "shops";
            try {
                database.createCollection(collectionName);
            } catch (Exception e) {
                System.out.println("Collection already exists: " + collectionName);
                System.out.println(database.getCollection(collectionName).deleteMany(new BsonDocument()));
            }
            for (CSVRecord record : records) {
                Document shop = new Document();
                shop.append("name", record.get(1));
                Double[] latLon = {Double.valueOf(record.get(2)), Double.valueOf(record.get(3))};
                List<Double> latitudeLongitude = Arrays.asList(latLon);
                Document point = new Document().append("type", "Point");
                point.append("coordinates", latitudeLongitude);
                shop.append("geopoint", point);
                shopsList.add(shop);
            }
            database.getCollection(collectionName).insertMany(shopsList);
            System.out.println(database.getCollection(collectionName).createIndex(Indexes.geo2dsphere("geopoint.coordinates")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Document createPoint(int latIndex, int lonIndex, CSVRecord record) {
        Double[] latLon = {Double.valueOf(record.get(latIndex)), Double.valueOf(record.get(lonIndex))};
        List<Double> latitudeLongitude = Arrays.asList(latLon);
        Document point = new Document().append("type", "Point");
        point.append("coordinates", latitudeLongitude);
        return point;
    }

    private void extractStops(String path, MongoDatabase database) {
        String collectionName = "stops";
        int latIndex = 7;
        int lonIndex = 6;
        try (Reader in = new FileReader(path + crimesPath)){
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            List<Document> stopsList = new ArrayList<>();
            try {
                database.createCollection(collectionName);
            } catch (Exception e) {
                System.out.println("Collection already exists: " + collectionName);
                System.out.println(database.getCollection(collectionName).deleteMany(new BsonDocument()));
            }
            for (CSVRecord record : records) {
                Document document = new Document();
                Document point = createPoint(latIndex, lonIndex, record);
                document.append("geopoint", point);
                stopsList.add(document);
            }
            database.getCollection(collectionName).insertMany(stopsList);
            System.out.println(database.getCollection(collectionName).createIndex(Indexes.geo2dsphere("geopoint.coordinates")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
